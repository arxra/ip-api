# Basic rest API

## Goals

This api returns all nonlocal-IPv4 adresses on the host as a json list when
called at $HOST:3030/backip . 
It does set the allow_all cors header in case you want to run this on a single
machine.

## Interop

This backend is made to answer calls from 
[this basic Svelte frontend](https://gitlab.com/arxra/ip-front).