FROM rust:1.41 as builder
WORKDIR /usr/src/backenduru
COPY ./Cargo.toml ./Cargo.lock ./
RUN mkdir src/
RUN echo "fn main() { }" > src/main.rs
RUN cargo build --release

RUN rm ./target/release/deps/backenduru*

COPY . .

RUN cargo build --release

FROM rust:1.41-slim
COPY --from=builder /usr/src/backenduru/target/release/backenduru /usr/local/bin/backenduru
CMD ["backenduru"]
