use ifaces;

pub fn return_ip<'a>() -> Vec<String> {
    let mut li: Vec<String> = Vec::new();

    for iface in ifaces::Interface::get_all().unwrap() {
        match iface.kind {
            ifaces::Kind::Ipv4 => {
                let ip = iface.addr.unwrap().ip().to_string();
                if !ip[..3].eq("127") {
                    li.push(ip);
                }
            }
            _ => {}
        }
    }

    return li;
}
