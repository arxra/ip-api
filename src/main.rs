use handlers::return_ip;
use warp::{reply::json, Filter};

mod handlers;

#[tokio::main]
async fn main() {
    let ip = [0, 0, 0, 0];
    let port = 3030;
    let cors = warp::cors().allow_any_origin();
    let hello = warp::path!("backip").map(|| json(&return_ip())).with(cors);

    println!("Hosting on {:?}:{}", ip, port);
    warp::serve(hello).run((ip, port)).await;
}
